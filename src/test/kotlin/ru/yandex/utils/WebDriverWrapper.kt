package ru.yandex.utils

import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.firefox.FirefoxDriver

object WebDriverWrapper {

    fun getDriver(browserName: String) : WebDriver {
        when (browserName.toLowerCase()) {
            "chrome" -> return ChromeDriver()
            "firefox" -> return FirefoxDriver()
            else -> return ChromeDriver()
        }
    }

}
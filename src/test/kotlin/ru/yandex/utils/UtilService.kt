package ru.yandex.utils

import java.io.FileInputStream
import java.io.IOException
import java.util.Properties

object UtilService {
    private val pathToProperty = "config.properties"
    private var properties: Properties? = null

    private fun loadProperties(): Unit {
        try {
            properties = Properties()
            properties?.load(FileInputStream(pathToProperty))
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    fun getProperties(properties: String): String {
        loadProperties()
        return UtilService.properties?.getProperty(properties).toString()
    }

}
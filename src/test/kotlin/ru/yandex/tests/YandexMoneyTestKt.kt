package ru.yandex.tests

import io.kotlintest.matchers.boolean.shouldBeTrue
import io.kotlintest.should
import org.openqa.selenium.support.ui.ExpectedConditions
import ru.yandex.utils.UtilService

class YandexMoneyTestKt : BaseTest() {

    private val username = UtilService.getProperties("ru.yandex.money.username")
    private val password = UtilService.getProperties("ru.yandex.money.password")

    init {
        "Пользователь логинится" {
            mainPage.clickOnLoginButton()
            passportPage.performUserLogin(username, password)
            homePage.clickOnUserInfo()
            homePage.userInfoIsDisplayed().shouldBeTrue()
            homePage.getUserName().should { equals(username) }
        }

        "Пользователь разлогинивается" {
            homePage.logoutFromAccount()
            wait.until(ExpectedConditions.visibilityOf(mainPage.promoBanner))
            mainPage.mainPageIsDisplayed().shouldBeTrue()
        }
    }
}
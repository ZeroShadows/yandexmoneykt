package ru.yandex.tests

import io.kotlintest.Description
import io.kotlintest.Spec
import io.kotlintest.TestResult
import io.kotlintest.extensions.TestListener
import io.kotlintest.specs.StringSpec
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.support.ui.WebDriverWait
import ru.yandex.pages.HomePage
import ru.yandex.pages.MainPage
import ru.yandex.pages.PassportPage
import ru.yandex.utils.UtilService
import ru.yandex.utils.WebDriverWrapper
import java.util.concurrent.TimeUnit

abstract class BaseTest : StringSpec(), TestListener {

    private val browserName: String = UtilService.getProperties("ru.yandex.money.browser")

    private val driver: WebDriver = WebDriverWrapper.getDriver(browserName)
    protected val homePage: HomePage = HomePage(driver)
    protected val mainPage: MainPage = MainPage(driver)
    protected val passportPage: PassportPage = PassportPage(driver)
    protected val wait: WebDriverWait = WebDriverWait(driver, 5)

    init {
        driver.manage()?.window()?.maximize()
        driver.manage()?.timeouts()?.implicitlyWait(5, TimeUnit.SECONDS)
    }

    override fun beforeSpec(description: Description, spec: Spec) {
        super<StringSpec>.beforeSpec(description, spec)
        mainPage.open()
    }

    override fun afterSpec(description: Description, spec: Spec) {
        super<StringSpec>.afterSpec(description, spec)
        driver.close()
    }


}
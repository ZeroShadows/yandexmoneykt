package ru.yandex.pages

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory
import org.openqa.selenium.support.ui.WebDriverWait

class HomePage(private val driver: WebDriver) {

    init {
        PageFactory.initElements(driver, this)
    }

    @FindBy(className = "qa-user-show-sidebar-avatar")
    private lateinit var userInfo: WebElement

    @FindBy(css = ".qa-user-exit button")
    private lateinit var logoutButton: WebElement

    @FindBy(css = "div.Text__StyledTextDiv-sc-3yi2jf-1.jBjjyd")
    private lateinit var userNameFromUserInfo: WebElement

    fun userInfoIsDisplayed(): Boolean {
        return userInfo.isDisplayed;
    }

    fun clickOnUserInfo() {
        userInfo.click()
    }

    fun logoutFromAccount() {
        logoutButton.click()
    }

    fun getUserName(): String {
        return userNameFromUserInfo.text
    }


}
package ru.yandex.pages

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory

class PassportPage(private val driver: WebDriver) {

    init {
        PageFactory.initElements(driver, this)
    }

    @FindBy(xpath = "//input[@name='login']")
    private lateinit var loginField: WebElement

    @FindBy(xpath = "//input[@name='passwd']")
    private lateinit var passwordField: WebElement

    @FindBy(xpath = "//button[@type='submit']")
    private lateinit var submitButton: WebElement

    @FindBy(className = "logo logo_name")
    private lateinit var passportLogo: WebElement

    private fun enterUsername(username: String) = loginField.sendKeys(username)

    private fun enterPassword(password: String) = passwordField.sendKeys(password)

    private fun clickSubmitButton() = submitButton.click()

    fun performUserLogin(username: String, password: String) {
        enterUsername(username)
        clickSubmitButton()
        enterPassword(password)
        clickSubmitButton()
    }

    fun passportPageIsDisplayed(): Boolean = passportLogo.isDisplayed
}
package ru.yandex.pages

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory
import org.openqa.selenium.support.ui.WebDriverWait
import ru.yandex.utils.UtilService

class MainPage(private val driver: WebDriver) {

    private val url = UtilService.getProperties("ru.yandex.money.link")

    init {
        PageFactory.initElements(driver, this)
    }

    @FindBy(className = "header2__button")
    lateinit var loginButton: WebElement

    @FindBy(className = "promo-screen__content")
    lateinit var promoBanner: WebElement


    fun open() = driver.get(url)

    fun clickOnLoginButton() = loginButton.click()

    fun mainPageIsDisplayed(): Boolean {
        return promoBanner.isDisplayed
    }

}